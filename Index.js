// Here we get all the elements from the html file
const loanButton = document.getElementById("loanButton")
const bankButton = document.getElementById("bankButton")
const workButton = document.getElementById("workButton")
const buyButton = document.getElementById("buyButton")
const myBalance = document.getElementById("balance")
const myPay = document.getElementById("pay")
const myLoan = document.getElementById("loan")
const myLaptops = document.getElementById("laptops")
const myName = document.getElementById("name")
const myImage = document.getElementById("image")
const myDescription = document.getElementById("description")
const myPrice = document.getElementById("price")

// Initializing some variables
let num = 200
myBalance.innerHTML = num
let pay = 0;
myPay.innerHTML = pay
let loan = 0;
let laptops = []

let laptopToBuy = null

// Here we get the data from this address using promises
fetch("https://hickory-quilled-actress.glitch.me/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptops(laptops))


// we add in the laptop struct the information of each laptop and initializing the information of the first laptop to appear
const addLaptops = (laptops) => {
    laptops.forEach(x => addLaptop(x))
    myPrice.innerText = laptops[0].price
    myDescription.innerText = laptops[0].description
    myName.innerText = laptops[0].title
    laptopToBuy = laptops[0]
    myImage.src = "https://hickory-quilled-actress.glitch.me/" + laptops[0].image
}

// here we get each laptop from its id and we take its data
const addLaptop = (laptop) => {
    const myLaptop = document.createElement("option")
    myLaptop.value = laptop.id
    myLaptop.appendChild(document.createTextNode(laptop.title))
    myLaptops.appendChild(myLaptop)
}

// here we make it so when we choose each laptop the information changes to match each laptop
const handleMenuChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex]
    myPrice.innerText = selectedLaptop.price
    myDescription.innerText = selectedLaptop.description
    myName.innerText = selectedLaptop.title
    myImage.src = "https://hickory-quilled-actress.glitch.me/" + selectedLaptop.image
    laptopToBuy = selectedLaptop
}

// an event listener to run the above function whenever we change laptop in our list
myLaptops.addEventListener("change", handleMenuChange);{

}

// in this function we take a loan if we dont have one and make sure we cant get another loan before we repay the first one
function Balance(){
    if(loan === 0){
        window.alert("Input the loan you want")
        desiredLoan = window.prompt("How much loan do you want?")
        if(desiredLoan > num * 2){
            window.alert("You cant get a loan")
        }
        else{
            loan = desiredLoan
            myLoan.innerHTML = "Loan: " + loan
            if(loan > 0){
                let repayButton = document.createElement("button")
                repayButton.setAttribute("id", "repayButton")
                repayButton.innerHTML = "Repay"
                repayButton.addEventListener("click", Repay)
                document.body.appendChild(repayButton)
            }
        }
    }
    else {
        window.alert("You cant get another loan")
    }
}

// Here we increase our money
function Work(){
    pay = pay + 100
    myPay.innerHTML = pay
}

// here if we have a loan then some of our money we go to repay the loan or else all the money will go to our balance
function Bank(){
    if(loan > 0){
        loan = loan - (1/10)*pay
        myLoan.innerHTML = loan
        pay = pay - (1/10)*pay
        myPay.innerHTML = pay
    }
    num = num + pay
    pay = 0;
    myBalance.innerHTML = num
    myPay.innerHTML = pay
}

// a function for repaying the loan 
function Repay(){
    if(pay > loan){
        repayLoan = pay - loan
        tempLoan = pay - repayLoan
        pay = pay - loan
        loan = loan - tempLoan
        myLoan.innerText = "Loan: " + loan
        myPay.innerText = pay
    }
    else{
        loan = loan - pay
        pay = 0
        myLoan.innerHTML = "Loan: " + loan
        myPay.innerHTML = pay
    }
}

// a function for buying the laptops
function Buy(){
        if(laptopToBuy.price > num + loan){
            window.alert("You cant afford this laptop")
        }
        else{
            num = num - laptopToBuy.price + +loan
            loan = 0
            myBalance.innerText = num
            myLoan.innerText = "Loan: " + loan
            window.alert("You are the owner of a new laptop")
        }
}

// some event listeners that work when we press the buttons calling the respective functions 
loanButton.addEventListener("click", Balance)
bankButton.addEventListener("click", Bank)
workButton.addEventListener("click", Work)
buyButton.addEventListener("click", Buy)